package de.boden.RegenRadarBonn;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;

public class Settings extends Activity {
	
	Button buttonOK;
	RadioButton rbuttonGif;
	RadioButton rbuttonWebm;
	
	LinearLayout layoutGif;
	LinearLayout layoutWebm;
	
	Boolean webm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		webm = prefs.getBoolean("webm", false);
		
//		Bundle bundle = getIntent().getExtras();
//		webm = bundle.getBoolean("webm", false);
		
		buttonOK = (Button) findViewById(R.id.buttonOK);
		rbuttonGif = (RadioButton) findViewById(R.id.radioButtonGIF);
		rbuttonWebm = (RadioButton) findViewById(R.id.radioButtonWEBM);
		layoutGif = (LinearLayout) findViewById(R.id.layoutgif);
		layoutWebm = (LinearLayout) findViewById(R.id.layoutwebm);
		
		if (webm) {
			rbuttonGif.setChecked(false);
			rbuttonWebm.setChecked(true);
		} else {
			rbuttonGif.setChecked(true);
			rbuttonWebm.setChecked(false);
		}
		
		//We will handle this via the layout listeners
		rbuttonGif.setClickable(false);
		rbuttonWebm.setClickable(false);
		
		layoutGif.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (rbuttonWebm.isChecked()) { rbuttonWebm.setChecked(false); }
				if (!rbuttonGif.isChecked()) { rbuttonGif.setChecked(true); }	
			}
		} );
		
		layoutWebm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (rbuttonGif.isChecked()) { rbuttonGif.setChecked(false); }
				if (!rbuttonWebm.isChecked()) { rbuttonWebm.setChecked(true); }		
			}
		} );
		
		buttonOK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (rbuttonWebm.isChecked()) 
					{ webm = true; } 
				else 
					{ webm = false; }
				Intent i = new Intent(getApplicationContext(), RegenRadarBonnActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
		.putBoolean("webm", webm)
		.commit();
	}

}
