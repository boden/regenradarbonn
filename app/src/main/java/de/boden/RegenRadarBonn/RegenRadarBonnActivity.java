package de.boden.RegenRadarBonn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class RegenRadarBonnActivity extends Activity {
	/** Called when the activity is first created. */

	String TAG = "RegenRadarBonn";

	WebView webView;
	VideoView vidView;
	TextView statusText;

	AlertDialog alertDialog;
	ProgressDialog progressDialog;
	AlertDialog connectionDialog;

	ArrayList<RadarType> radarTypes = new ArrayList<RadarType>();

	int type;
	Boolean shortRange;
	Boolean webm;
	Boolean juelich;
	Boolean error = false;
	Boolean newAndroidVersionDetected = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setupRadarTypes();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		type = prefs.getInt("type", 0);
		shortRange = prefs.getBoolean("shortRange", false);
		webm = prefs.getBoolean("webm", false);
		newAndroidVersionDetected = prefs.getBoolean("newAndroidVersionDetected", false);
		juelich = prefs.getBoolean("juelich", false);

		if (!newAndroidVersionDetected) { checkVersion(); }

		if (webm) {
			setContentView(R.layout.vidview);
			vidView = (VideoView) findViewById(R.id.videoView1);
			vidView.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					//Log.v(TAG, "Loop completed, restarting VideoView");
					if (!error) {
						mp.start();
					}
				}
			});

			vidView.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					if (progressDialog != null && progressDialog.isShowing()) {
						progressDialog.cancel();
					}
					Log.v(TAG, "Video data loaded, starting VideoView");
					mp.start();
				}
			});

			vidView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
					Log.e(TAG, "Error in MediaPlayer... aborting");
					error = true;
					if (progressDialog != null && progressDialog.isShowing()) {
						progressDialog.cancel();
					}
					return false;
				}
			});

		} else {

			getWindow().requestFeature(Window.FEATURE_PROGRESS);

			setContentView(R.layout.webview);
			webView = (WebView) findViewById(R.id.webView1);
			webView.getSettings().setLoadWithOverviewMode(true);
			webView.getSettings().setUseWideViewPort(true);
			webView.getSettings().setBuiltInZoomControls(true);
			webView.setWebViewClient(new WebViewClient() {
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return super.shouldOverrideUrlLoading(view, url);
				}
			});
			webView.setWebChromeClient(new WebChromeClient() {
				public void onProgressChanged(WebView view, int progress) {
					RegenRadarBonnActivity.this.setProgress(progress * 100);
				}
			});

		}

		statusText = (TextView) findViewById(R.id.statusText);

	}

	private void checkVersion() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			Log.v(TAG, "Android 4.4 or newer detected ... setting app to WebM mode.");
			webm = true;
			newAndroidVersionDetected = true;
		}
	}

	private void setupRadarTypes() {

		RadarType reflectivity = new RadarType();
		reflectivity.name = getString(R.string.reflectivity);
		reflectivity.pathGif50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zh_ani.gif";
		reflectivity.pathGif100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zh_ani.gif";
		reflectivity.pathWebm50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zh_bonn.webm";
		reflectivity.pathWebm100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zh_bonn.webm";
		reflectivity.pathGif50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zh_juelich.ani.gif";
		reflectivity.pathGif100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zh_juelich.ani.gif";
		reflectivity.pathWebm50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zh_juelich.webm";
		reflectivity.pathWebm100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zh_juelich.webm";
		radarTypes.add(reflectivity);

		RadarType doppler = new RadarType();
		doppler.name = getString(R.string.doppler);
		doppler.pathGif50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_v_ani.gif";
		doppler.pathGif100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_v_ani.gif";
		doppler.pathWebm50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_vh_bonn.webm";
		doppler.pathWebm100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_vh_bonn.webm";
		doppler.pathGif50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_vh_juelich.ani.gif";
		doppler.pathGif100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_vh_juelich.ani.gif";
		doppler.pathWebm50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_vh_juelich.webm";
		doppler.pathWebm100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_vh_juelich.webm";
		radarTypes.add(doppler);

		RadarType diffref = new RadarType();
		diffref.name = getString(R.string.diffreflectivity);
		diffref.pathGif50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zdr_ani.gif";
		diffref.pathGif100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zdr_ani.gif";
		diffref.pathWebm50 = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zdr_bonn.webm";
		diffref.pathWebm100 = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zdr_bonn.webm";
		diffref.pathGif50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zdr_juelich.ani.gif";
		diffref.pathGif100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zdr_juelich.ani.gif";
		diffref.pathWebm50JL = "https://www2.meteo.uni-bonn.de/data/radar/az_50_zdr_juelich.webm";
		diffref.pathWebm100JL = "https://www2.meteo.uni-bonn.de/data/radar/az_100_zdr_juelich.webm";
		radarTypes.add(diffref);

	}

	private String getUrl() {
		return radarTypes.get(type).getPath(webm, shortRange, juelich);
	}

	private String getDescription() {
		return radarTypes.get(type).getDescription(webm, shortRange, juelich);
	}

	@Override
	public void onResume() {
		super.onResume();

		ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

		if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
			if (webm) {
				showVideo(getUrl());
			} else {
				webView.loadUrl(getUrl());
			}

			new CheckRadarDate().execute(getUrl());
			Toast.makeText(getApplicationContext(), getDescription(), Toast.LENGTH_SHORT).show();

		} else {
			connectionDialog = new AlertDialog.Builder(this).create();
			connectionDialog.setMessage(getString(R.string.error_message_connection));
			connectionDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				} });
			connectionDialog.show();
			Log.v(TAG, "No internet connection... aborting");

		}

	}

	private void showVideo(final String uriString) {
		Log.v(TAG, "Setting video URI to " + uriString);
		vidView.suspend();
		vidView.setVideoURI(Uri.parse(uriString));
		if (!error) {
			progressDialog = new ProgressDialog(this);
			progressDialog.setMessage(getString(R.string.loading));
			progressDialog.show();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (webm) {
			vidView.suspend();
		} else {
			webView.loadUrl("about:blank");
		}

		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
				.putInt("type", type)
				.putBoolean("webm", webm)
				.putBoolean("shortRange", shortRange)
				.putBoolean("newAndroidVersionDetected", newAndroidVersionDetected)
				.putBoolean("juelich", juelich)
				.commit();

		if (alertDialog != null && alertDialog.isShowing()) {
			alertDialog.cancel();
		}

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.cancel();
		}

		if (connectionDialog != null && connectionDialog.isShowing()) {
			connectionDialog.cancel();
		}
	}

	protected void onDestroy() {
		if (webm) {
			vidView.stopPlayback();
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.reload:
				if (webm) {
					showVideo(getUrl());
				} else {
					webView.reload();
				}
				new CheckRadarDate().execute(getUrl());
				Toast.makeText(getApplicationContext(), getDescription(), Toast.LENGTH_SHORT).show();
				return true;
			case R.id.type:
				changeType();
				return true;
			case R.id.reach:
				changeReach();
				return true;
			case R.id.location:
				changeLocation();
				return true;
			case R.id.webm:
				changeWebmGif();
				return true;
			case R.id.about:
				showAboutDialogue();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void changeWebmGif() {

		Intent i = new Intent(getApplicationContext(), Settings.class);
		startActivity(i);
	}

	private void changeType() {
		if (radarTypes.size()-1 > type) {
			type++;
		} else {
			type = 0;
		}

		if (webm) {
			showVideo(getUrl());
		} else {
			webView.loadUrl(getUrl());
		}

		new CheckRadarDate().execute(getUrl());
		Toast.makeText(getApplicationContext(), getDescription(), Toast.LENGTH_SHORT).show();

	}

	private void changeReach() {

		if (shortRange) { shortRange = false; }
		else { shortRange = true; }

		if (webm) {
			showVideo(getUrl());
		} else {
			webView.loadUrl(getUrl());
		}

		new CheckRadarDate().execute(getUrl());
		Toast.makeText(getApplicationContext(), getDescription(), Toast.LENGTH_SHORT).show();
	}

	private void changeLocation() {

		if (juelich) { juelich = false; }
		else { juelich = true; }

		if (webm) {
			showVideo(getUrl());
		} else {
			webView.loadUrl(getUrl());
		}

		new CheckRadarDate().execute(getUrl());
		Toast.makeText(getApplicationContext(), getDescription(), Toast.LENGTH_SHORT).show();

	}

	private void showAboutDialogue() {
		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Info");
		alertDialog.setMessage(Html.fromHtml("<p>Regenradar Bonn zeigt aktuelle Niederschläge im Großraum "
				+ "Bonn bzw. Jülich an (Umkreis max. 100 KM). Die Darstellung erfolgt in Form von animierten Radarbildern im Gif- oder Webm-Format, "
				+ "die die Entwicklung der Niederschläge jeweils über den Zeitraum der letzten 2 Stunden "
				+ "visualisieren.</p>"
				+ "<p>Die dargestellten Radarbilder stammen von der Webseite des Metereologischen Institus der "
				+ "Universität Bonn und des Forschungszentrums Jülich. "
				+ "Da es sich dabei um einen freiwilligen Service handelt, kann leider keine permanente "
				+ "Verfügbarkeit des Dienstes garantiert werden. Aktuelle Informationen zum Radar und möglichen "
				+ "Störungen: http://www.meteo.uni-bonn.de/messdaten/radarbilder</p>"
				+ "Die Zeitangaben oben auf dem Radarbild sind UTC, für Bonn/Jülich (MEZ) müssen daher ein bzw. zwei Stunden dazugerechnet werden. "
				+ "Weitere Informationen zum Radar und zur Interpretation der "
				+ "Radarbilder: http://www.meteo.uni-bonn.de/messdaten/radarbilder/informationen-zum-radar</p>"
				+ "<p>RegenradarBonn ist Open Source Software, der Quellcode der App ist hier verfügbar: "
				+ "https://bitbucket.org/boden/regenradarbonn/src/master</p>"));
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				return;
			} });
		alertDialog.show();
		TextView message = (TextView) alertDialog.findViewById(android.R.id.message);
		//message.setTextColor(Color.WHITE); // Workaround to prevent dialogue text to change color when touched.
		Linkify.addLinks(message, Linkify.WEB_URLS);
	}


	class CheckRadarDate extends AsyncTask<String, Void, String> {

		//Checks webserver to find out the date of the displayed radar film. Then checks if the date is current and
		// issues a warning to the user if radar data is older than 2h.

		protected String doInBackground(String... radarurl) {
			Log.v("RegenRadarBonn", "Checking data timestamps on server ...");
			String date = "";

			try {
				URL url = new URL("https://www2.meteo.uni-bonn.de/data/radar/");
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				String radarname = (String) radarurl[0].subSequence(42, radarurl[0].length());
				String str;
				while ((str = in.readLine()) != null) {
					if (str.contains(radarname)) {
						try {
							date = (String) str.subSequence((str.indexOf("</a></td><td align=\"right\">") + 27),
									str.indexOf("  </td>"));
							Log.v("RegenRadarBonn", "Found corresponding date to " + radarname + ": " + date);
						} catch (Exception e) {
							Log.v("RegenRadarBonn", e.toString());
						}
						break;
					}
				}
				in.close();
			} catch (MalformedURLException e) {
				Log.e("RegenRadarBonn", e.toString());
			} catch (IOException e) {
				Log.e("RegenRadarBonn", e.toString());
			}

			return date;

		}

		protected void onProgressUpdate(Integer... progress) {
			// Not needed here.
		}

		@SuppressLint("SimpleDateFormat")
		protected void onPostExecute(String date) {

			String statusString = "";

			if (date.length() > 0) { // If this is not set, pulling server data likely failed. In this case we don't display anything.

				Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
				SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH); // This seems to change ... that's bad.
				sdfInput.setTimeZone(TimeZone.getTimeZone("UTC"));
				Date currentDate = instance.getTime();
				Date radarDate = new Date();

				try {
					radarDate = sdfInput.parse(date);
				} catch (ParseException e) {
					Log.e("RegenRadarBonn", "Timestamp could not be recognized. " + e.toString());
					statusText.setTextColor(Color.RED);
					statusText.setText(getString(R.string.status_error));
					return;
				}

				Log.v("RegenRadarBonn", "Radar Date: " + radarDate.toString() + ", Current Date: " + currentDate.toString());

				SimpleDateFormat sdfOutput = new SimpleDateFormat("dd.MM.yyyy, HH:mm z");

				StringBuilder sb = new StringBuilder();
				String location = "Bonn";
				if (juelich) location = "Jülich";

				if (currentDate.getTime() > (radarDate.getTime() + 30*60*1000)) {
					statusText.setTextColor(Color.RED);
					sb.append(location);
					sb.append(": ");
					sb.append(sdfOutput.format(radarDate));
					sb.append(" (!)");
					statusString = sb.toString();
					//statusString = getString(R.string.status) + " " + sdfOutput.format(radarDate) + " (!)";
					Toast.makeText(getApplicationContext(), getString(R.string.status_warning), Toast.LENGTH_LONG).show();
					Log.e("RegenRadarBonn", "Houston, we have a problem! Radar data seems to be older than 30 mins...");
				} else {
					statusText.setTextColor(Color.BLACK);
					int difference = (int) ((currentDate.getTime()/60000) - (radarDate.getTime()/60000));
					sb.append(location);
					sb.append(": vor ");
					sb.append(difference);
					sb.append(" Minute(n)");
					statusString = sb.toString();
					//statusString = getString(R.string.status) + " vor " + difference + " Minute(n)";
					//statusString = getString(R.string.status) + " " + sdfOutput.format(radarDate);
					Log.v("RegenRadarBonn", "Radar data seems to be current. All in order. :)");
				}

			}

			statusText.setText(statusString);
		}

	}

}