package de.boden.RegenRadarBonn;

public class RadarType {
	
	public String name;
	public String pathGif50;
	public String pathWebm50;
	public String pathGif100;
	public String pathWebm100;
	public String pathGif50JL;
	public String pathWebm50JL;
	public String pathGif100JL;
	public String pathWebm100JL;
		
	public String getPath(Boolean webm, Boolean shortRange, Boolean juelich) {
		if (webm) { 
			if (shortRange) {
				if (juelich) {
					return pathWebm50JL;
				} else {
					return pathWebm50;
				}
			} else {
				if (juelich) {
					return pathWebm100JL;
				} else {
					return pathWebm100;
				}
			}
		} else {
			if (shortRange) {
				if (juelich) {
					return pathGif50JL;
				} else {
					return pathGif50;
				}
			} else {
				if (juelich) {
					return pathGif100JL;
				} else {
					return pathGif100;
				}
			}
		}
	}
	
	public String getDescription(Boolean webm, Boolean shortRange, Boolean Juelich) {
		if (webm) { 
			if (shortRange) {
				if (Juelich) {
					return name + " Jülich - 50Km (WebM)";
				} else {
					return name + " Bonn - 50Km (WebM)";
				}
			} else {
				if (Juelich) {
					return name + " Jülich - 100Km (WebM)";
				} else {
					return name + " Bonn - 100Km (WebM)";
				}
			}
		} else {
			if (shortRange) {
				if (Juelich) {
					return name + " Jülich - 50Km (GIF)";
				} else {
					return name + " Bonn - 50Km (GIF)";
				}
			} else {
				if (Juelich) {
					return name + " Jülich - 100Km (GIF)";
				} else {
					return name + " Bonn - 100Km (GIF)";
				}
			}
		}
	}
	
	
}
