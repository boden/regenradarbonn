Diese einfache Android-App zeigt frei verfügbare Wetterinformationen des Metereologischen Instituts der Universität Bonn und des Forschungszentrums Jülich.

Die App ist auch im Goole Play Store erhältlich: [https://play.google.com/store/apps/details?id=de.boden.RegenRadarBonn](https://play.google.com/store/apps/details?id=de.boden.RegenRadarBonn)